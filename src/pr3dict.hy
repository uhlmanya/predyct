(import os datetime time sys)
(import [pathlib [Path]])
(import [pandas :as pd])
(import [pandas.plotting [scatter_matrix]])
(import [seaborn :as sns])
(import [numpy :as np])
(import [scipy :as sp])
(import [gnuplotlib :as gp])
(import [matplotlib :as mpl])
(import [matplotlib.pyplot :as plt])
(import [matplotlib [cm]])
(import [mpl_toolkits.mplot3d [axes3d]])
(import joblib)
(import [sklearn.preprocessing [QuantileTransformer]])
(import [sklearn.model_selection._split [train_test_split]])
(import torch)
(import [torch.nn :as nn])
(import [torch.nn.functional :as func])
(import [torch.optim :as optim])
(import [torch.utils.data.dataset [TensorDataset]])
(import [torch.utils.data.dataloader [DataLoader]])
(import [torch.utils.tensorboard [SummaryWriter]])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])

(setv eng (.EngFormatter mpl.ticker))

; Neural Network Class
(defclass predict-net [nn.Module]
    (defn --init-- [self num-ips num-ops]
        (.--init-- (super predict-net self))
        (setv self.fc-1 (.Linear nn num-ips 128))
        (setv self.fc-2 (.Linear nn 128 256))
        (setv self.fc-3 (.Linear nn 256 512))
        (setv self.fc-4 (.Linear nn 512 1024))
        (setv self.fc-5 (.Linear nn 1024 512))
        (setv self.fc-6 (.Linear nn 512 256))
        (setv self.fc-7 (.Linear nn 256 128))
        (setv self.fc-8 (.Linear nn 128 num-ops)))
    (defn forward [self x]
        (->> x (.fc-1 self) (.relu func)
               (.fc-2 self) (.relu func)
               (.fc-3 self) (.relu func) 
               (.fc-4 self) (.relu func)
               (.fc-5 self) (.relu func)
               (.fc-6 self) (.relu func)
               (.fc-7 self) (.relu func)
               (.fc-8 self) (.relu func))))

;; Handle arguments
(setv device-name (if (<= (len sys.argv) 1) 
                      "nmos1v" 
                      (get sys.argv 1)))

(cond [(= device-name "nmos1v")
        (setv device-type "n")]
      [(= device-name "pmos1v")
        (setv device-type "p")]
      [True
        (raise (ValueError (.format "Wrong device-name: {0}" device-name)))])

;; Filesystem Setup
(setv device-db (+ "../data/" device-name ".h5"))
(setv model-dir (+ "./model/" device-name "/"
                (.strftime (.now datetime.datetime) "%Y%m%d-%H%M%S")))
(setv prefix (+ model-dir "/" device-name))
(.mkdir (Path model-dir) :parents True :exist-ok True)

;; If running interactively
(setv check-point model-dir)

;; Read Data Frame
(setv data-frame (as-> device-db df
                       (.read-hdf pd df "database")
                       (get df (.all (.isfinite np df) 1))
                       (.dropna df)))

;; Tensorboard setup
(setv summary (SummaryWriter (+ model-dir "/tf-logs")))

;; CUDA setup
(setv device (.device torch (if (.is-available torch.cuda) "cuda" "cpu")))
(print f"We're running on a {(.get-device-name torch.cuda)}")

(setv input-cols ["P_l" "V_D" "V_G" "V_B"])
(setv output-cols ["OPP_id" "OPP_fug"
                   "OPP_vth" "OPP_vdsat"
                   "OPP_gm" "OPP_gds" "OPP_gmb"
                   "OPP_cgd" "OPP_cgs" "OPP_cgb"
                   "OPP_cds" "OPP_cdb" "OPP_csb"])

;; Normalize Data
(setv input-raw (. (get data-frame input-cols) values))
(setv output-raw (. (get data-frame output-cols) values))

(setv input-transformer (QuantileTransformer :output-distribution "uniform" 
                                             :random-state 666))
(setv output-transformer (QuantileTransformer :output-distribution "uniform" 
                                              :random-state 666))

(setv input-data (.fit-transform input-transformer input-raw))
(setv output-data (.fit-transform output-transformer output-raw))

(.dump joblib input-transformer (+ prefix ".input"))
(.dump joblib output-transformer (+ prefix ".output"))

;; Setup Training Data
(setv batch-size 1000)
(setv test-split 0.3)

(setv (, X-train
         X-valid
         Y-train
         Y-valid) (train-test-split input-data
                                    output-data
                                    :test-size test-split
                                    :shuffle True
                                    :random-state 42))

(setv train-set (TensorDataset (.Tensor torch X-train)
                               (.Tensor torch Y-train)))
(setv valid-set (TensorDataset (.Tensor torch X-valid) 
                               (.Tensor torch Y-valid)))

(setv train-loader (DataLoader train-set batch-size :pin-memory True))
(setv valid-loader (DataLoader valid-set batch-size :pin-memory True))

;; Define network
(setv dev-net (.to (predict-net (len input-cols) 
                                (len output-cols)) 
                   device))

(.add-graph summary dev-net (.to (.zeros torch [1 (len input-cols)]) device))

(setv loss-mse (.MSELoss nn))
(setv loss-mae (.L1Loss nn))

;(setv optim-adamw (.AdamW optim (.parameters dev-net)))
(setv optim-adam (.Adam optim (.parameters dev-net)))

;; Training Process
(defn forward-train [X-train Y-train &optional [optim optim-adam]]
    (let [_ (.zero-grad optim)
          out-train (dev-net X-train)
          mse (loss-mse out-train Y-train)
          mae (loss-mae out-train Y-train)]
        (.backward mse)
        (.step optim)
        (, (.item mse) (.item mae))))

(defn forward-valid [X-valid Y-valid]
    (let [out-valid (dev-net X-valid)
          mse (loss-mse out-valid Y-valid)
          mae (loss-mae out-valid Y-valid)]
        (, (.item mse) (.item mae))))

(defn epoch-step [epoch-count]
    (let [t0 (.replace (.now datetime.datetime) :microsecond 0)
          _ (.train dev-net)
          loss-accumulator (fn [loader]
                            (reduce (fn [acc err] 
                                      (tuple (lfor x (zip acc err) (sum x))))
                                    (lfor (, X-train Y-train) loader
                                          (forward-train (.to X-train device)
                                                         (.to Y-train device)))
                                    (, 0 0)))
          train-err (loss-accumulator train-loader)
          valid-err (with [(.no-grad torch)]
                      (.eval dev-net)
                      (loss-accumulator valid-loader))
          train-mse-avg (/ (first train-err) (len train-loader))
          train-mae-avg (/ (second train-err) (len train-loader))
          valid-mse-avg (/ (first valid-err) (len valid-loader))
          valid-mae-avg (/ (second valid-err) (len valid-loader))
          t1 (.replace (.now datetime.datetime) :microsecond 0)
          epc-str (.format "[{0} - {1}] Epoch {2}/{3}: " 
                           t0 t1 (inc epoch-count) num-epochs)
          mse-str (.format "Training MSE: {0}" (eng train-mse-avg))
          mae-str (.format "Validation MAE: {0}" (eng valid-mae-avg))]
        (print (+ epc-str "\n\t" mse-str "\n\t" mae-str))
        (, train-mse-avg valid-mae-avg)))

(setv num-epochs 42)

(setv (, train-losses valid-losses)
        (loop [[epoch 0] 
               [best-candidate None] 
               [train-losses (list)] 
               [valid-losses (list)]]
            (let [error (epoch-step epoch)
                  train-mse (first error)
                  valid-mae (second error)
                  candidate (if (or (is best-candidate None) 
                                    (> best-candidate valid-mae))
                                valid-mae
                                best-candidate)]
                (when (not (= candidate best-candidate))
                    (.save torch (.state-dict dev-net) (+ prefix ".model"))
                    (print (.format "\tNew Candidate Net saved!")))
                (.add-scalar summary :tag "Train_MSE" 
                                     :global-step epoch 
                                     :scalar-value train-mse)
                (.add-scalar summary :tag "Valid_MAE" 
                                     :global-step epoch 
                                     :scalar-value valid-mae)
                (if (< epoch num-epochs)
                    (recur (inc epoch) 
                           candidate
                           (+ train-losses [train-mse])
                           (+ valid-losses [valid-mae]))
                    (, train-losses valid-losses)))))

;; Plot and evaluate Progress
(setv epochs (.array np (range 0 num-epochs)))

(.plot gp (, epochs (.array np train-losses) {"legend" "MSE"})
          (, epochs (.array np valid-losses) {"legend" "MAE"})
          :-with "lines" :terminal "dumb 70,40" :unset "grid")

;(setv fig (.figure plt :figsize (.figaspect plt 0.4)))
;(setv ax-train (.add-subplot fig 1 2 1))
;(.plot ax-train (range num-epochs) train-losses)
;(.set-xlabel ax-train "#Epoch")
;(.set-ylabel ax-train "Training MSE")
;(.set-title ax-train "Training Loss")
;(setv ax-valid (.add-subplot fig 1 2 2))
;(.plot ax-valid (range num-epochs) valid-losses)
;(.set-xlabel ax-valid "#Epoch")
;(.set-ylabel ax-valid "Validation MAE")
;(.set-title ax-valid "Validation Loss")
;(.show plt)

;; Validation
(defn predict [input-data batch-size &kwonly [ckpt-prefix False]]
    (let [net (predict-net (len input-cols) (len output-cols))
          input-trafo (.load joblib (+ (or ckpt-prefix prefix) ".input"))
          output-trafo (.load joblib (+ (or ckpt-prefix prefix) ".output"))
          input-transformed (.transform input-trafo input-data)
          input-set (TensorDataset (.Tensor torch input-transformed))
          input-loader (DataLoader input-set batch-size)]
        (.to net device)
        (.load-state-dict net (.load torch (+ (or ckpt-prefix prefix) ".model")))
        (.eval net)
        (.reshape 
            (.array np 
                (lfor batch input-loader
                    (as-> batch it
                        (first it)
                        (.to it device)
                        (net it)
                        (.to it "cpu")
                        (.numpy it.data)
                        (.inverse-transform output-trafo it))))
            num-samples (len output-cols))))

(defn plot-id [id2d-tru id2d-prd vo-tru vg-tru &kwonly [save False]]
    (let [fig (.figure plt :figsize (, 4 18)) ; :figsize (.figaspect plt 0.3)
          XY (.meshgrid np vo-tru vg-tru)
          X (first XY)
          Y (second XY)
          spline (.Rbf sp.interpolate X Y id2d-prd :function "quintic" 
                                                   :smooth 2)
          id2d-spl (spline X Y)
          id2d-err (.abs np (- id2d-prd id2d-tru))
          ax-tru (.add-subplot fig 4 1 1 :projection "3d")
          ax-prd (.add-subplot fig 4 1 2 :projection "3d")
          ax-err (.add-subplot fig 4 1 3 :projection "3d")
          ax-spl (.add-subplot fig 4 1 4 :projection "3d")]
        (.plot-surface ax-tru X Y id2d-tru :rstride 8 
                                           :cstride 8 
                                           :alpha 0.3 
                                           :cmap cm.winter)
        (.contour ax-tru X Y id2d-tru :zdir "z" :offset 0 :cmap cm.Accent)
        (.contour ax-tru X Y id2d-tru :zdir "y" :offset 0 :cmap cm.Accent)
        (.contour ax-tru X Y id2d-tru :zdir "x" :offset 0 :cmap cm.Accent)
        (.set-xlabel ax-tru "$V_{ds}$ [V]")
        (.set-ylabel ax-tru "$V_{gs}$ [V]")
        (.set-zlabel ax-tru "$J_{d}$ [A/m]")
        (.set-title ax-tru "Ground Truth")
        (.plot-surface ax-prd X Y id2d-prd :rstride 8 
                                           :cstride 8 
                                           :alpha 0.3 
                                           :cmap cm.winter)
        (.contour ax-prd X Y id2d-prd :zdir "z" :offset 0 :cmap cm.Accent)
        (.contour ax-prd X Y id2d-prd :zdir "y" :offset 0 :cmap cm.Accent)
        (.contour ax-prd X Y id2d-prd :zdir "x" :offset 0 :cmap cm.Accent)
        (.set-xlabel ax-prd "$V_{ds}$ [V]")
        (.set-ylabel ax-prd "$V_{gs}$ [V]")
        (.set-zlabel ax-prd "$J_{d}$ [A/m]")
        (.set-title ax-prd "Raw Prediction")
        (.plot-surface ax-err X Y id2d-err :rstride 8 
                                           :cstride 8 
                                           :alpha 0.3 
                                           :cmap cm.winter)
        (.contour ax-err X Y id2d-err :zdir "z" :offset 0 :cmap cm.Accent)
        (.contour ax-err X Y id2d-err :zdir "y" :offset 0 :cmap cm.Accent)
        (.contour ax-err X Y id2d-err :zdir "x" :offset 0 :cmap cm.Accent)
        (.set-xlabel ax-err "$V_{ds}$ [V]")
        (.set-ylabel ax-err "$V_{gs}$ [V]")
        (.set-zlabel ax-err "$\Delta J_{d}$ [A/m]")
        (.set-title ax-err "Prediction Error")
        (.plot-surface ax-spl X Y id2d-spl :rstride 8 
                                           :cstride 8 
                                           :alpha 0.3 
                                           :cmap cm.winter)
        (.contour ax-spl X Y id2d-spl :zdir "z" :offset 0 :cmap cm.Accent)
        (.contour ax-spl X Y id2d-spl :zdir "y" :offset 0 :cmap cm.Accent)
        (.contour ax-spl X Y id2d-spl :zdir "x" :offset 0 :cmap cm.Accent)
        (.set-xlabel ax-spl "$V_{ds}$ [V]")
        (.set-ylabel ax-spl "$V_{gs}$ [V]")
        (.set-zlabel ax-spl "$J_{d}$ [A/m]")
        (.set-title ax-spl "Smooth Prediction")
        (when save 
            (.savefig plt "nmos1v.pdf" 
                      :type "pdf" 
                      :transparent True 
                      :pad-inches 0))
        (.show plt)))

(defn eval-plot [&kwonly [check-point False]
                         [device-type "n"]]
    (setv num-samples 59)
    (setv l-tru 3e-7)
    (setv vb-tru 0.0)

    (setv data-tru (get (get data-frame (& (= (get data-frame "P_l") l-tru)
                                           (= (get data-frame "V_B") vb-tru)))
                        (+ input-cols ["OPP_id"])))

    (setv VG-tru (. (get data-tru "V_G") values))
    (setv VO-tru (. (get data-tru "V_D") values))
    (setv ID2D-tru (.empty np (, 0 num-samples)))
    (setv ID2D-prd (.empty np (, 0 num-samples)))

    (setv i-d (fn [x] 
                (get (predict x (first x.shape) :ckpt-prefix check-point)
                     (, (slice None) 
                        (.index output-cols "OPP_id")))))

    (for [vg (.unique np VG-tru)]
        (let [input-data (-> data-tru
                             (get (= data-tru.V_G vg))
                             (get input-cols)
                             (. values))
              id-tru (-> data-tru
                         (get (= data-tru.V_G vg))
                         (get "OPP_id")
                         (. values))
              id-prd (i-d input-data)]
            (setv ID2D-tru (.vstack np (, ID2D-tru id-tru)))
            (setv ID2D-prd (.vstack np (, ID2D-prd id-prd)))))

    (plot-id ID2D-tru ID2D-prd (.unique np VO-tru) (.unique np VG-tru) :save True)
    )

(setv check-point prefix)
(setv check-point "./model/nmos1v/20201124-114605/nmos1v")
model/nmos1v/20201124-114605
(setv device-name "nmos1v")
(setv device-type "n")

(eval-plot :check-point check-point :device-type "n")


;(setv df-out-trn (.DataFrame pd :data output-data :columns output-cols))
;(setv df-out-tru (.DataFrame pd :data output-raw :columns output-cols))

(setv tru-sample (.sample (get data-frame (> data-frame.OPP_id 0))
                        :n 10000))

(setv trn-sample (.DataFrame pd :data (.transform output-transformer 
                                                  (get tru-sample output-cols)) 
                                :columns output-cols))

(setv classes (lfor (, idx dat) (.iterrows tru-sample)
                    (if (> dat.V_D (- dat.V_G dat.OPP_vth))
                        "triode" "saturation")))

(-> (.jointplot sns :x "OPP_id" :y "OPP_gm" 
                    :data tru-sample 
                    :kind "reg" 
                    :color "k"
                    ;:scatter False
                :scatter_kws {"s" 5})
    (.set-axis-labels "$J_{d}$ [A/m]" "$g_{m}/W$ [S/m]"))
;(.scatterplot sns :x "OPP_id" :y "OPP_gm" :data tru-sample :hue classes)

(-> (.jointplot sns :x "OPP_id" :y "OPP_gm" :data trn-sample :kind "reg" :color "k"
                :scatter_kws {"s" 5})
    (.set-axis-labels "$J_{d}$ (normalized)" "$g_{m}$ (normalized)"))


(.show plt)

(scatter-matrix tru-sample 
                :alpha 0.2 
                :figsize (, 6 6) 
                :diagonal "kde" 
                :c "black"
                :hist-kwds {"color" ["black"]})
(scatter-matrix trn-sample 
                :alpha 0.2 
                :figsize (, 6 6) 
                :diagonal "kde" 
                :c "black"
                :hist-kwds {"color" ["black"]})


